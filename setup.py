from setuptools import setup

setup(
    name="interventions",
    version="0.2",
    description="Light-Weight Models for Impact and Cost of Interventions",
    url="http://github.com/kimetrica/darpa/interventions",
    license="MIT",
    packages=["interventions"],
)
