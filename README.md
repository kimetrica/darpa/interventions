# Intervention Modeling

Development in this repository addresses the impact and cost modelling 
components of the Crisis Toolkit. It may eventually be merged with the 
repository for the [Interventions API](
https://intervention.kimetrica.com/admin).

## Repository Contents

1. interventions (python package)
    1. [impact](interventions/impact/README.md) (python subpackage)
    1. [cost](interventions/cost/README.md) (python subpackage)
    1. util (python subpackage)
1. setup.py (python packaging with setuptools)

## Overview

For the purpose of intervention modeling, an `Intervention` is an activity aiming to directly perturb a variable in a geographic location over a period of time. The Intervention module is a stand alone model that can ingest indicator datasets generated from domain models. In [classDiagram](https://mermaid-js.github.io/mermaid/#/classDiagram) form, the `Intervention` class declares those three attributes.

```mermaid
classDiagram
    Intervention
    class Intervention {
        +param location
        +param period
        +param variable
    }
```

The foundation of Kimetrica’s approach to intervention modeling are the postulates that:

1. only a small number of classes inherit directly from `Intervention`, and
2. interventions inheriting from this small number of classes can be modeled with the same families of algorithms and training data.

Currently the intervention module offers the "Provide" and "Build" classes, for distribution of aid/food and improvement of infrastructure, respectively. As in Python programming, a class that inherits from these is a specialized case of the general class.

```mermaid
classDiagram
    Intervention <|-- Provide
    Intervention <|-- Build
    class Intervention {
        +param location
        +param period
        +param variable
    }
```

Unlike Python programming, the next level in the class hierarchy has a semantic meaning. The english sentence "the WFP provided food aid to 3 million households in Oromia from March-June 2017" has "food aid" as a [direct object](https://www.collinsdictionary.com/us/dictionary/english/direct-object) of the verb "provide".

```mermaid
classDiagram
    Intervention <|-- Provide
    ProvideType <|-- FoodAid: direct object
    ProvideType <|-- CashTransfer: direct object
    ProvideType <|-- FertilizerDistribution: direct object
    class Intervention {
        +param location
        +param period
        +param variable
    }
```

Moreover, the sentence above also has elements that map to attributes of the class. In the `Provide` class, we expect at least three parameters for the "scale" of the intervention.
```mermaid
classDiagram
    Intervention <|-- Provide
    ProvideType <|-- FoodAid: direct object
    ProvideType <|-- CashTransfer: direct object
    ProvideType <|-- FertilizerDistribution: direct object
    class Intervention {
        +param location
        +param period
        +param variable
    }
    class ProvideType {
		+float number_of_recipients
		+float number_of_units
        +float quantity_per_unit
	}
```

When a `FoodAid` object is instantiated, the numbers (e.g. 3 million households) are fed in to represent an __actual__ or __hypothetical__ intervention.

Lastly, what distinguishes `FoodAid` from the other interventions inheriting from `ProvideType`? To describe this, it is necessary to bring in what an instance of an intervention does: they calculate an impact on the variable, and they calculate a cost of the intervention. The hypothesis is that we can define the algorithims\equations used on `ProvideType` and parameters\training data on it's children.

```mermaid
classDiagram
    Intervention <|-- Provide
    ProvideType <|-- FoodAid: direct object
    ProvideType <|-- CashTransfer: direct object
    ProvideType <|-- FertilizerDistribution: direct object
    class Intervention {
        +param location
        +param period
        +param variable
    }
    class ProvideType {
		+float number_of_recipients
		+float number_of_units
        +float quantity_per_unit
        +impact(a, b, ...)
        +cost(x, y, ...)
	}
    class FoodAid {
        +param a
        +param b
        +param x
        +param y
    }
```

Anyway, most of this stuff should eventually be taken care of by the "Interventions API", a work in progress. Asking Python classes to represent ontologies is not good architecture, but it is done in this repository as a "patch" until an ontology service is working. The bulk of work for developing models is getting an upper hand on the variables on which interventions act and the algorithims/equations and training data needed to build the impact and cost methods. That said, improvements to this Python-classes-as-ontology system may help.

## Variables

TODO: Overview.

Current variables that have fed into an intervention model:

1. Malnutrition Model output as been fed into the "free_food_distribution" intervention for `Provide` class.
2. Road network model can be fed into the "upgrade" intervention type for `Build` class

## Impact

TODO: Overview. See impact/readme.md.

## Cost

TODO: Overview. See cost/readme.md.

## Future

A brief list of major "To Do" items:

1. Connect source of normalized Intervention variables (Model Service?)
1. Connect source of normalized Indicator data (Indicator API?)
1. Resolve destination for output of impact and cost estimates (Model Service?)
1. Test & improve the (guessed, process based) impact estimates
1. Test & improve the (machine learning based) cost estimates
