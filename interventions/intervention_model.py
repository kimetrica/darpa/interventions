import pickle
from functools import partial

import boto3
import geopandas as gpd
import pyproj
from shapely.geometry import MultiLineString, MultiPoint, Point
from shapely.ops import nearest_points, transform

from .cost.util_functions import Intervention_cost
from .impact.util_functions import intervention_impact

########## Intervention (provide) nested class #########

# This class calls on the impact function and cost function


class Intervention_provide(object):
    def __init__(
        self,
        indicator_col="gam_number",
        admin_level="admin2",
        provide_type="Free food distribution",
        period_col="start",
        increase=True,
    ):
        """
        define parameters intrinsic to the provide intervention itself.
        increase: the direction of the intervention impact. True means intervention will decrease indicator value such as cases of malnutrition (default set to True).
        These parameters are scale invariant, which means they are independent of the scale of the intervention instance.

        Example of running this module:
        free_food=Intervention_provide(alpha1=0., beta1=0.91, beta2=0.2, beta3=0.8,
                                    delta1=0.,gamma2=0.7, gamma3=0.2,)
        impact_effect, impact_cost = free_food.impact_calculation(source_fname=maln_fname,
                                                        q1=1000, q2=4, q3_norm=1,
                                                        q3_1=2,q3_2=0,
                                                        q3_3=0, q3_4=0,
                                                        q3=2,
                                                        interv_window=7,
                                                        reporting_dur=60,
                                                        model_fname=g)
        """

        self.indicator_col = indicator_col
        self.admin_level = admin_level
        self.provide_type = provide_type
        self.period_col = period_col
        self.increase = increase

        if self.provide_type == "Free food distribution":
            self.TE = 1
            self.alpha1 = 0.1
            self.beta1 = 0.9
            self.beta2 = 0.8
            self.beta3 = 0.45
            self.gamma2 = 0.2
            self.gamma3 = 0
            self.delta1 = 0
        elif self.provide_type == "Supplementary feeding":
            self.TE = 1
            self.alpha1 = 0.1
            self.beta1 = 0.595
            self.beta2 = 0.5
            self.beta3 = 0.3
            self.gamma2 = 0.2
            self.gamma3 = 0
            self.delta1 = 0
        elif self.provide_type == "School feeding":
            self.TE = 1
            self.alpha1 = 0.1
            self.beta1 = 0.05
            self.beta2 = 0.04
            self.beta3 = 0.025
            self.gamma2 = 0.2
            self.gamma3 = 0
            self.delta1 = 0
        elif self.provide_type.lower() == "vaccine":
            self.TE = 1
            self.alpha1 = 0.0
            self.beta1 = 0.91
            self.beta2 = 0.03
            self.beta3 = 0
            self.gamma2 = 0
            self.gamma3 = 0
            self.delta1 = 0.0

    def impact_calculation(
        self,
        source_fname,
        q1,
        q2,
        q3,
        q3_norm,
        q3_1,
        q3_2,
        q3_3,
        q3_4,
        interv_window,
        reporting_dur,
    ):
        """
        This calculates impact and cost, given scale of the intervention
        (e.g., number of recipients, number of rounds, etc)
        source_fname: file directory or directory path to file
        q1: number of recipients (integer)
        q2: frequency, or number of intervention events (integer). It needs to be less than reporting_dur
        q3: quantity of treatment/transaction per recipient in one round of intervention
        q3_norm: normalized dose/treatment delivery efficiency.
        interv_window: number of days between interventions
        model_fname: S3 file path of pickle object for trained cost model (e.g., https://darpa-output-dev.s3.amazonaws.com/final_targets/cost1_reg_output_4c717b2e0d.pickle)
        """
        ######################
        ## calculate impact ##
        ######################
        intervention_instance = intervention_impact(
            period_col=self.period_col,
            indicator_col=self.indicator_col,
            provide_type=self.provide_type,
            admin_level=self.admin_level,
            increase=self.increase,
            q1=q1,
            q2=q2,
            q3=q3,
            q3_norm=q3_norm,
            TE=self.TE,
            alpha1=self.alpha1,
            beta1=self.beta1,
            beta2=self.beta2,
            beta3=self.beta3,
            gamma2=self.gamma2,
            gamma3=self.gamma3,
            delta1=self.delta1,
            interv_window=interv_window,
            reporting_dur=reporting_dur,
        )

        impact_effect = intervention_instance.calc_impact(source_fname)

        ####################
        ## calculate cost ##
        ####################

        s3 = boto3.resource("s3")
        j = "cost_model.pickle"
        model_fname = "https://darpa-output-dev.s3.amazonaws.com/final_targets/cost1_reg_output_4c717b2e0d.pickle"
        f_name = "/".join(model_fname.split("/")[-2:])
        with open(j, "wb") as data:
            s3.Bucket("darpa-output-dev").download_fileobj(f_name, data)

        cost_mod = pickle.load(open(j, "rb"))  # noqa: S301

        interv_cost = Intervention_cost(cost_mod, self.provide_type)

        impact_cost = interv_cost.provide_cost_calculation(
            # mod_obj=cost_mod,
            # interv_type=self.provide_type,
            q1=q1,
            q2=q2,
            q3=q3,
            q3_1=q3_1,
            q3_2=q3_2,
            q3_3=q3_3,
            q3_4=q3_4,
        )

        return impact_effect, impact_cost


########## Intervention (build) class #########
class Intervention_build(object):
    def __init__(
        self, road_shapefile, coords, provide_type="upgrade",
    ):
        """
        provide_type: the type of intervention.
        For instance, "upgrade" means repair or upgrade of roads in poor or very poor condition
        "add link" means adding new roads.
        road_shapefile: input geopandas dataframe where the geometry is a LineString type
        coords: specifed as a list like [( (x_0, y_0),(x_1, y_1) )], where x=longitude and y= latitude.
        It can be just one point but generalizable to multistring points like [(x,y)].
        Note that the first tuple should (x_0, y_0) be the starting point.
        """

        self.provide_type = provide_type
        self.road_shapefile = road_shapefile
        self.coords = coords

    def impact_length(self):
        """
        This calculates the closes road to a point specified by x and y
        """
        if isinstance(self.road_shapefile, gpd.GeoDataFrame):
            if self.provide_type == "upgrade":
                assert type(self.coords[0][-1]) == float

                #################################################
                ## calculate length of road under intervention ##
                #################################################

                point = Point(self.coords[0])
                distance = self.road_shapefile.distance(point)
                min_dist_idx = distance.idxmin()
                line_polygon = self.road_shapefile.iloc[min_dist_idx]["geometry"]

            elif self.provide_type == "add link":
                assert type(self.coords[0][-1]) == tuple
                # make a new line based on user specified coordinates
                starting_point = Point(self.coords[0][0])

                # find the vertex on the existing road network that's closest to starting_point

                distance = self.road_shapefile.distance(starting_point)
                min_dist_idx = distance.idxmin()
                line_geom = self.road_shapefile.iloc[min_dist_idx]["geometry"]
                line_mp = MultiPoint(line_geom.coords)
                nearest_np = nearest_points(line_mp, starting_point)[0]
                nearest_pt_coord = (
                    nearest_np.coords.xy[0][0],
                    nearest_np.coords.xy[1][0],
                )

                # substitute `nearest_pt_coord` for the first point in self.coords, while keeping the rest the same

                revised_coords = []
                for i in range(len(self.coords)):
                    if i == 0:
                        j = (nearest_pt_coord, self.coords[i][-1])
                        revised_coords.append(j)
                    elif i > 0:
                        revised_coords.append(self.coords[i])

                line_polygon = MultiLineString(revised_coords)

            else:
                return
        else:
            print("wrong file type")
            return

        # convert from distance deg to meter

        project = partial(
            pyproj.transform, pyproj.Proj("EPSG:4326"), pyproj.Proj("EPSG:32633")
        )
        polygon_transformed = transform(project, line_polygon)
        dist = polygon_transformed.length

        return dist

    def quality_upgrade(self, target_cond):
        label2int = {
            "excellent": 0,
            "good": 1,
            "fair": 2,
            "poor": 3,
            "very_poor": 4,
        }
        if self.provide_type == "upgrade":
            assert type(self.coords[0][-1]) == float

            ######################
            ## determine quality ##
            ######################
            if isinstance(self.road_shapefile, gpd.GeoDataFrame):
                point = Point(self.coords[0])
                distance = self.road_shapefile.distance(point)
                min_dist_idx = distance.idxmin()
                orig_cond = self.road_shapefile.iloc[min_dist_idx]["road_pred"]

                orig_cond_score = label2int[orig_cond]
                target_cond_score = label2int[target_cond]
                upgrade_range = orig_cond_score - target_cond_score
                if upgrade_range < 0:
                    print("this is a downgrade, try another target quality value")

                return upgrade_range

        elif self.provide_type == "add link":
            """
            This simply returns the quality of the added new road
            """
            assert type(self.coords[0][-1]) == tuple

            target_cond_score = label2int[target_cond]
            return target_cond_score

    def impact_calculation(self, target_cond):
        """
        model_fname: url of the model artifact (e.g.https://darpa-output-dev.s3.amazonaws.com/final_targets/cost_intervention/coefficients_estimate_99914b932b.pickle)
        """
        dist = self.impact_length()
        target_cond_score = self.quality_upgrade(target_cond)

        s3 = boto3.resource("s3")
        j = "build_cost_model.pickle"
        model_fname = "https://darpa-output-dev.s3.amazonaws.com/final_targets/cost_intervention/coefficients_estimate_99914b932b.pickle"

        f_name = "/".join(model_fname.split("/")[3:])
        with open(j, "wb") as data:
            s3.Bucket("darpa-output-dev").download_fileobj(f_name, data)

        cost_mod = pickle.load(open(j, "rb"))  # noqa: S301

        interv_cost = Intervention_cost(cost_mod, self.provide_type)
        dist_km = dist / 1000
        cost = interv_cost.build_cost_calculation(dist_km, target_cond_score)

        return dist, cost
