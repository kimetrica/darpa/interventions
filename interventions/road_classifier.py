import boto3
import geopandas as gpd
import numpy as np
import rasterio
from rasterio.mask import mask

import os
import zipfile
import osgeo.gdal as gdal
from osgeo import osr

from keras.models import load_model

import descarteslabs as dl

raster_client = dl.Raster()

#### helper functions #####


def image_preprocess(aoi_geo):

    scenes_binary, ctx_binary = dl.scenes.search(
        aoi_geo,
        products=["sentinel-2:L1C"],
        start_datetime="2020-01-01",
        end_datetime="2020-06-01",
        limit=20,
        cloud_fraction=0.05,
    )

    select_scene = scenes_binary[-1].properties.id
    s2_arr_wgs84, m_wgs84 = raster_client.ndarray(
        select_scene,
        srs="EPSG:4326",  # WGS84 (lat-lon)
        # 0.0000046 corresponds to 50cm in decimal degrees,
        # 0.0001 corresponds to 10m
        resolution=0.009,
        bands=["red", "green", "blue", "derived:ndvi"],
        data_type="UInt16",
    )
    if not os.path.exists("inference_images"):
        os.makedirs("inference_images")

    dst_filename = "inference_images/test_sample.tif"

    x_pixels = s2_arr_wgs84.shape[1]  # number of pixels in x
    y_pixels = s2_arr_wgs84.shape[0]  # number of pixels in y
    driver = gdal.GetDriverByName("GTiff")
    dst_ds = driver.Create(
        dst_filename, x_pixels, y_pixels, 4, gdal.GDT_UInt16
    )  # GDT_Byte is Uint8, use UInt16 as default

    r_pixels = s2_arr_wgs84[:, :, 0]
    g_pixels = s2_arr_wgs84[:, :, 1]
    b_pixels = s2_arr_wgs84[:, :, 2]
    ndvi = s2_arr_wgs84[:, :, 3]

    geotransform = m_wgs84["geoTransform"]
    dst_ds.SetGeoTransform(geotransform)
    # specify coords
    srs = osr.SpatialReference()  # establish encoding
    srs.ImportFromEPSG(4326)  # WGS84 lat/long
    dst_ds.SetProjection(srs.ExportToWkt())  # export coords to file
    dst_ds.GetRasterBand(1).WriteArray(r_pixels)  # write r-band to the raster
    dst_ds.GetRasterBand(2).WriteArray(g_pixels)  # write g-band to the raster
    dst_ds.GetRasterBand(3).WriteArray(b_pixels)  # write b-band to the raster
    dst_ds.GetRasterBand(4).WriteArray(ndvi)  # write ndvi band to the raster
    dst_ds.FlushCache()  # write to disk
    dst_ds = None


def image_masking_crop(raster_fname, nl_fname, mask_polygon):

    # crop the raster with polygon,
    try:
        with rasterio.open(raster_fname) as src:
            # mask only takes sequence of polygon
            cropped_image, out_transform = mask(
                src, [mask_polygon], nodata=0, crop=False
            )  # crop=false to maintain same size

    except IOError:
        print(f"skipped: {raster_fname}")
        return

    ###### Append the raster array from night lights ######
    road_data = rasterio.open(raster_fname)
    ds = gdal.Open(nl_fname)

    ds = gdal.Translate(
        "/vsimem/new3.tif",
        ds,
        projWin=[
            road_data.bounds[0],
            road_data.bounds[3],
            road_data.bounds[2],
            road_data.bounds[1],
        ],
        height=road_data.height,
        width=road_data.width,
    )
    nl_arr = np.expand_dims(ds.ReadAsArray(), axis=2)

    cropped_image = np.moveaxis(cropped_image, 0, -1)

    data_arr = np.concatenate((cropped_image, nl_arr), axis=-1)

    if data_arr.shape[0] > 110:

        data_arr = data_arr[0:110, :, :]
    if data_arr.shape[1] > 110:
        data_arr = data_arr[:, 0:110, :]

    return data_arr


def data_compile(geom_shp):
    "compiles data from multiple polygons"

    data_compiled = []

    for i, v in geom_shp.iterrows():
        g = v["buff_geometry"].__geo_interface__
        nl_fname = "/vsis3/darpa-output-dev/night_light_2015_east_africa.tif"
        raster_fname = "inference_images/test_sample.tif"
        image_preprocess(g)
        data_arr = image_masking_crop(raster_fname, nl_fname, g)
        data_compiled.append(data_arr)

    data_output = np.array(data_compiled)
    return data_output


def model_inference(data_arr, model_checkpoint, stand_np):

    stand_arr = (data_arr[:] - stand_np[0, :]) / stand_np[1, :]
    saved_model = load_model(model_checkpoint)

    prediction = saved_model.predict(x=stand_arr, batch_size=64)

    return prediction.argmax(axis=1)


########### terminal task function  ##############


def road_condition_infer(input_gpd, model_file):
    """
    This function takes a shapefile input and
    queries a Sentinel2 satellite and runs inference
    to predict the road surface condition.

    input_gpd: geopandas dataframe from shapefile
    model_file: a url or file path to the model object(e.g. https://darpa-output-dev.s3.amazonaws.com/model_v1_2.zip)
    """
    s3 = boto3.resource("s3")
    j = "road_classifier_model.zip"
    f_name = model_file.split("/")[-1]

    with open(j, "wb") as data:
        s3.Bucket("darpa-output-dev").download_fileobj(f_name, data)

    with zipfile.ZipFile("road_classifier_model.zip", "r") as zip_ref:
        zip_ref.extractall("model_checkpoint")

    if isinstance(input_gpd, gpd.GeoDataFrame):
        input_gpd.drop_duplicates(subset=["geometry"], inplace=True)
        input_gpd["buff_geometry"] = input_gpd["geometry"].buffer(distance=0.01)
        processed_data = data_compile(input_gpd)
        norm_vectors = np.array(
            [
                [
                    1.27552814e01,
                    1.09971762e01,
                    1.06526346e01,
                    4.89450745e02,
                    1.53160855e-01,
                ],
                [
                    1.46056137e02,
                    1.21645630e02,
                    1.17568634e02,
                    4.44728369e03,
                    9.97659445e-01,
                ],
            ],
            dtype="float32",
        )

        model_pred = model_inference(
            processed_data, "model_checkpoint/model_v1_2/", norm_vectors
        )
        pred_result = model_pred.tolist()
        input_gpd["surface_pred"] = pred_result
        label_dict = {0: "excellent", 1: "fair", 2: "good", 3: "poor", 4: "very_poor"}
        # a limitation of shapefiles that they have a limit of 10 characters
        input_gpd["surface_pred"] = input_gpd["surface_pred"].map(label_dict)
        return input_gpd
    else:
        print("wrong input format")
        return
