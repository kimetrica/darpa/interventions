from urllib.parse import urlparse, urlunparse
import numpy as np

from kiluigi import FinalTarget


def float_data(data_var):
    """helper for json.load to convert `None` in 'data' array to np.nan"""

    if "data" in data_var:
        try:
            data_var["data"] = np.array(data_var["data"]).astype(np.float)
        except ValueError:
            pass

    return data_var


def serializable(obj):
    """helper for json.dump to make certain objects serializable"""
    # TODO: make compatible with LocalTarget backend

    if hasattr(obj, "__geo_interface__"):
        return obj.__geo_interface__
    if isinstance(obj, FinalTarget):
        parts = list(urlparse(obj.path))
        parts[0] = "https"
        parts[1] = f"{parts[1]}.s3.amazonaws.com"
        return {"@id": urlunparse(parts)}
    return str(obj)
