from luigi import Config
from luigi.parameter import BoolParameter, ListParameter, Parameter, FloatParameter


class ProvideType(Config):
    """
    The generic parameters for "provisioning" type interventions characterize
    the "scale" (i.e. intensity or effort) of a putative intervention.
    """

    number_of_recipients = ListParameter(
        default=[0],
        description=(
            "List the total number of unique recipients of the provisioned "
            "items. If a singleton, the number will be distributed across the "
            "location of the intervention. Otherwise, the dimension(s) must "
            "match the spatial dimensions of the indicator dataset."
        ),
    )
    number_of_rounds = ListParameter(
        default=[0],
        description=(
            "List the number of times each recipient is provisioned. If a "
            "singleton, the number will be distributed across the period "
            "of the intervention. Otherwise, the dimension must match the "
            "time dimension of the indicator dataset."
        ),
    )
    quantities_per_recipient_per_round = ListParameter(
        default=[0],
        description=(
            "List of the quantity of each item included in a provisioned "
            '"unit", the order aligns with the order of `item_coefficients` '
            "parameter of `ProvideConfig`."
        ),
    )


class Provide(Config):
    """
    The Provide configuration supplies numexpr strings in variables "x", "y",
    and "q".
    - "x" is the array of numbers of units distributed
    - "y" is the array of the unperturbed indicator
    - "q" is the vector of item quantities (per recipient per round)
    """

    type = Parameter(
        default="", description="Intervention type name as in intervention ontology."
    )
    additive = BoolParameter(
        default=True,
        description=(
            "Whether the impact on the indicator dataset is additive "
            "(default) or proportional."
        ),
    )
    increase = BoolParameter(
        default=True,
        description=(
            "Whether the direction of change for the indicator dataset is "
            "positive (default) or negative."
        ),
    )
    accuracy = Parameter(
        default="",
        description=(
            "A `numexpr` string in arrays `x`, the array of numbers of units "
            "distributed, and `y`, the array of the unperturbed indicator."
        ),
    )
    items = ListParameter(
        default=[],
        description=(
            "Coefficients to the item quantities whose inner product is used in"
            "the `efficacy` expression as `q`."
        ),
    )
    efficacy = Parameter(
        default="",
        description=(
            'A "dose response" function as a  `numexpr` string in arrays `x`, '
            "the array of numbers of units distributed, and `q`, the inner "
            "product of item coefficients and quantities."
        ),
    )
    persistence = FloatParameter(
        default=0,
        description=(
            "The persistence of impact over time, at the temporal resolution "
            "of the indicator dataset."
        ),
    )
