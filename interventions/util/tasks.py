from datetime import date

from kiluigi import Task, IntermediateTarget
from luigi.util import requires
import pandas as pd
import xarray as xr
import geopandas as gpd


class GetIndicatorDataset(Task):
    # TODO: use Indicator API, when exists

    def output(self):
        return IntermediateTarget(task=self)

    def run(self):
        """
        As a hack for not having a data lake/warehouse: implement a custom
        method for each indicator dataset you want that returns an
        xarray.Dataset with coordinates "period" and "location" and an
        indicator variable named "y". Overwrite this method, here for
        debugging purposes, with that one.
        """
        ds = xr.Dataset(
            {"y": (("period", "location"), [[1, 2, 3], [4, 5, 6]])},
            coords={"period": [date(2020, 1, 1), date(2020, 2, 1)]},
        )
        with self.output().open("w") as f:
            f.write(ds)


@requires(GetIndicatorDataset)
class AugmentIndicatorDataset(Task):
    def output(self):
        return IntermediateTarget(task=self)

    def run(self):

        with self.input().open() as f:
            ds = f.read()

        # calculate duration variable
        period = pd.Series(ds["period"])
        ds["duration"] = (
            "period",
            ((period + 1).dt.to_timestamp() - period.dt.to_timestamp()).dt.days,
        )

        # calculate area variable
        if hasattr(ds, "geometry"):
            gs = gpd.GeoSeries(ds["geometry"], crs={"init": "epsg:4326"})
            gs = gs.to_crs(epsg=32637)  # TODO: okay projection?
            area = ds["geometry"].copy()
            area.name = "area"
            area.values = gs.area / 10 ** 6
        else:
            # TODO: need area estimate for AOI in gridded data
            raise NotImplementedError
        ds = xr.merge([ds, area])

        with self.output().open("w") as f:
            f.write(ds)
