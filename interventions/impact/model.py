from pathlib import Path
import json

from geopandas import GeoDataFrame
from kiluigi import (
    Task,
    IntermediateTarget,
    FinalTarget,
)
from luigi import WrapperTask
from luigi.util import requires, inherits
import numexpr as ne
import numpy as np

from ..util.functions import serializable
from ..util.config import Provide, ProvideType
from ..util.tasks import AugmentIndicatorDataset


@requires(AugmentIndicatorDataset)
@inherits(ProvideType)
class RunProvide(Task):
    """
    Simulate the impact of a provisioning intervention on the indicator
    included in the prediction data, over the given location and period at
    the spatial and temporal resolution of the indicator.
    """

    # TODO: use exp/log if provide.additive is true ... correctly?

    def output(self):
        return IntermediateTarget(task=self)

    def run(self):

        # read in the indicator dataset
        with self.input().open() as f:
            ds = f.read()

        # queue up the Provide configuration
        provide = Provide()

        # prepare for calculations on units per recipient
        s = np.array(self.number_of_recipients)
        t = np.array(self.number_of_rounds)
        q = np.array(self.quantities_per_recipient_per_round)

        # distribute recipients across locations
        # TODO: provide options other than uniform
        p = ds["area"] / ds["area"].sum()
        s = s * p

        # distribute rounds across periods
        # TODO: provide options other than uniform
        p = ds["duration"] / ds["duration"].sum()
        t = p * t

        # units distributed each location and period
        x = t * s

        # load indicator variable into numpy array with shape ...
        #     (period, y, x) for raster, and
        #     (period, location) for vector.
        y = ds["y"]
        # TODO: fix nodata value upstream
        nodatavals = y.attrs.get("nodatavals", None)
        if nodatavals:
            y.load()
            y = y.where(y != nodatavals[0], np.nan)

        # "likelihood" that provisioned units reach recipients
        TL = ne.evaluate(provide.accuracy, local_dict={"x": x, "y": y})

        # effectiveness of provisioned unit in changing indicator
        q = q.dot(provide.items)
        EF = ne.evaluate(provide.efficacy, local_dict={"x": x, "q": q})

        # calculate weights for all time steps
        w = np.ones(y.shape)
        np.divide(
            np.diff(y, axis=0, prepend=0), y, out=w, where=y != 0,
        )
        w = w.clip(min=0)
        if provide.increase:  # TODO: check logic
            w = 1 - w

        # container for impact indicator (just getting shape)
        # z := 1 - y_star / y
        z = np.empty(y.shape)

        # initialize z value for stepping through t
        z_t = np.zeros(y.shape[1:], y.dtype)

        # solve discrete initial-value problem by iteration
        for t in range(y.shape[0]):
            w_t = w[t]
            p_t = TL[t] * EF[t] * int(provide.increase or -1)
            z_t = p_t + (1 - p_t) * provide.persistence * (1 - w_t) * z_t
            z[t] = z_t

        # the impacted indicator
        y.values = (1 - z) * y

        # the transformed input
        ds["y"] = y

        with self.output().open("w") as f:
            f.write(ds)


class RunBuild(Task):
    # FIXME: just here to show plan of multiple inherits on the
    #  RunIntervention task
    pass


@inherits(RunProvide, RunBuild)
class RunIntervention(WrapperTask):
    """
    One task to wrap them all and in a pipeline bind them.
    """

    # TODO: ensure parameter conflicts between different intervention classes
    #  don't break the approach of inheriting ALL parameters and letting
    #  luigi's clone method pass the correct ones.

    def requires(self):
        provide = Provide()
        if provide.type:
            # TODO: more robust way to choose?
            return self.clone(RunProvide)
        else:
            raise NotImplementedError


@requires(RunIntervention)
class Output(Task):
    """
    Prepare result of model runs for UI.
    """

    def output(self):
        p = Path(*self.task_id.split("."))
        return FinalTarget(path=str(p.with_suffix(".json")))

    def run(self):

        with self.requires().input().open() as f:
            ds = f.read()

        # for location with geometries
        location = ds.drop_dims("period")
        if "geometry" in location:
            location = GeoDataFrame(location.to_dataframe())
        else:
            # outdated code for raster data output
            #
            # # prepare gridded data
            # tiles = [
            #     FinalTarget(path=f"{p.values}.tiff", task=self)
            #     for p in ds["period"]
            #     ]
            # profile = ds[variable].attrs.copy()
            # profile.update(
            #     {
            #         "count": 1,
            #         "driver": "GTiff",
            #         "dtype": ds[variable].values.dtype,
            #         "nodatavals": np.nan,
            #         "width": ds.coords["x"].size,
            #         "height": ds.coords["y"].size,
            #         "bbox": None,
            #         }
            #     )
            # for i, target in enumerate(tiles):
            #     da = ds[variable].isel(period=i)
            #     with target.temporary_path() as path:
            #         with rio.open(path, "w", **profile) as dst:
            #             dst.write(da.values, 1)
            #             if not profile["bbox"]:
            #                 profile["bbox"] = list(dst.bounds)
            # ds.update({variable: ("period", tiles)})
            # profile["nodatavals"] = None
            # ds[variable].attrs.update(profile)
            # dd["groups"][k] = ds.to_dict()
            # da = dd["groups"][k]["data_vars"][variable]
            # da["tiles"] = da.pop("data")
            raise NotImplementedError
        dd = ds.drop_vars(location.columns).to_dict()
        dd["coords"]["location"] = location

        # enrich description
        dd["attrs"] = {
            "label": "Intervention Impact",
            "task": self.task_family,
            "parameters": self.to_str_params(),
        }

        dd.update(serializable(self.output()))
        with self.output().open("w") as f:
            # TODO: resolve inclusion of NaN in Python's json.dump
            json.dump(dd, f, default=serializable)
