import pandas as pd
import numpy as np
import os
import geopandas as gpd
import xarray as xr
import calendar
import rasterio
import glob


def daily_df(gdf, ffil_col=None, period_col="start", admin_col="admin2"):
    """
    returns the gdf with daily data point.
    gdf needs to have the time stamp column 'yr-month-date',
    https://stackoverflow.com/questions/50320709/pandas-filling-missing-dates-and-values-within-group-   with-duplicate-index-values/50321221
    """
    # enforce datetime dtype
    df = gdf.drop(["geometry"], axis=1)
    geo_feat = gdf[["geometry", admin_col]].drop_duplicates()

    df[period_col] = pd.to_datetime(df[period_col])
    try:
        filled_df = (
            df.set_index(period_col)
            .groupby(admin_col)
            .apply(
                lambda d: d.reindex(
                    pd.date_range(min(df[period_col]), max(df[period_col]), freq="MS")
                )
            )
            .drop(admin_col, axis=1)
            .reset_index([admin_col])
            .fillna(np.nan)
        )
        filled_df = filled_df.reset_index().rename(columns={"index": period_col})
        merged_df = filled_df.merge(geo_feat, on=[admin_col], how="inner").reset_index(
            drop=True
        )

        # assign value to missing time points that were filled forward
        merged_df["Year"] = merged_df[period_col].apply(lambda x: int(x.year))
        merged_df["Month"] = merged_df[period_col].apply(
            lambda x: calendar.month_name[int(x.month)]
        )

        if ffil_col:
            merged_df.loc[:, ffil_col] = merged_df.loc[:, ffil_col].ffill()

    except ValueError:
        merged_df = gdf
        merged_df[period_col] = pd.to_datetime(merged_df[period_col])

    # fill in the dates
    calendar_cols = ["Year", "Month", "date"]
    timestamp_df = pd.DataFrame(columns=calendar_cols)
    col_idx = merged_df.columns.tolist().index(period_col)
    for row in merged_df.itertuples():
        month = int(row[col_idx + 1].month)
        year = row.Year
        num_days = calendar.monthrange(year, month)[1]
        days = [
            (str(year) + "-" + str(month) + "-" + str(day))
            for day in range(1, num_days + 1)
        ]
        for day in days:

            timestamp_df = timestamp_df.append(
                {"Year": row.Year, "Month": row.Month, "date": day}, ignore_index=True
            )

    timestamp_df.drop_duplicates(inplace=True)
    gdf_daily = merged_df.merge(timestamp_df, on=["Year", "Month"], how="left")

    return gdf_daily


def day_assign(df, admin_col="admin2"):
    """
    For each admin, the function assigns the nth day based on sorted 'date' value
    """
    admin_ls = df[admin_col].unique().tolist()
    df_day = []
    for i in admin_ls:
        admin_df = df.loc[df[admin_col] == i].reset_index(drop=True)
        admin_df.loc[:, "day"] = admin_df.index
        df_day.append(admin_df)

    df_day = pd.concat(df_day, axis=0).reset_index(drop=True)
    return df_day


def te_dose_size(beta3, gamma3, q3_norm):

    te_dose_size = beta3 * (q3_norm ** gamma3)
    return te_dose_size


def te_decay(delta, te_current, duration):

    te_decay = te_current * ((1 - delta) ** duration)
    return te_decay


def te_initial(alpha1, beta1, beta3, gamma3, q3_norm, q3):

    te_initial = alpha1 + (beta1 ** (1 / q3) * te_dose_size(beta3, gamma3, q3_norm))
    return te_initial


def te_round(beta2, beta3, gamma2, gamma3, q2_current, q3_norm, te_current, q3):

    round_improvement = (
        (1 - te_current)
        * (beta2 + (gamma2 ** q2_current)) ** (1 / q3)
        * te_dose_size(beta3, gamma3, q3_norm)
    )

    te_round = te_current + round_improvement

    return te_round


def calc_percentage(src_df, feat_col, period_col=["Month", "Year"]):
    """
    calculates the new case percentage at a time interval after an intervention
    perturbs the indicator value.
    period_col is the list of time(e.g ["Month", "Year"], ["date"])
    """
    total_cases = (
        src_df.groupby(period_col)
        .agg([(feat_col, lambda x: x[x > 0].sum())])
        .reset_index()
    )
    case_percent = []
    col_idx = src_df.columns.tolist().index(feat_col)

    for row in src_df.itertuples():
        if period_col == ["Month", "Year"]:
            total_num = total_cases.loc[
                (total_cases["Month"] == row.Month) & (total_cases["Year"] == row.Year)
            ][feat_col].values[0]
        else:
            period_idx = src_df.columns.tolist().index(period_col)
            total_num = total_cases.loc[
                (total_cases[period_col] == row[period_idx + 1])
            ][feat_col].values[0]

        percent_cases = (
            row[col_idx + 1] / total_num
        )  # itertuples starts with index of 1
        case_percent.append(percent_cases)

    src_df.loc[:, "case_percent"] = case_percent
    return src_df


def TL_calc(Y, q1, TE):  # user input, default, or interventions data
    """
    Y is a an array for each time point!
    Linear response of provisioning success to coverage and target efficiency.
    The return value incorporates a default function mapping q_1 and TE
    to the range (0, 1).
    """

    if TE < -1 or TE > 1:
        raise Exception("parameter out of bounds")

    # assume q_1 targets evenly distributed in space over cases
    coverage = q1 / Y.sum(skipna=True).load().data

    if coverage > 1:
        coverage = 1

    assert coverage <= 1
    # if TE is 1, then coverage = coverage
    coverage = coverage * (0.5 + 0.5 * TE)
    return coverage


def distribution_intervention(input_xarr, increase, q1, TE):
    """
    A general function that calculates intervention impact for distributed goods (food) to a population on
    an admin level.
    the input has to be a xarray with geometry, timestamp, and y, and EF,  dose day.
    """
    xr_imp = input_xarr.copy()
    y = xr_imp["indicator"]

    # calculate weights for all time steps

    w = (np.diff(y, axis=0, prepend=0) / y).clip(min=0)
    z = np.zeros(y.shape)
    EF = input_xarr["EF"]
    # mask the -9999 values to np.nan for TL calculation
    y_masked = y.where(
        y != -9999.0,
    )
    #  calculate TL, the proportion of the recipients in a population that received intervention
    TL = []
    for t in range(y_masked.shape[0]):

        TL_val = TL_calc(y_masked[t], q1, TE)
        TL.append(TL_val)

    TL = xr.DataArray(np.array(TL))
    dose_day = input_xarr["dose day"].data
    # Through masking only show where dose_day is True, masking the dose day where it's False, and set it to zero.
    TL = TL.where(dose_day == True, 0)  # noqa: E712

    # initialize value for steps t, then calculate z
    z_t = np.zeros(y.shape[1:], y.dtype)
    for t in range(y.shape[0]):
        w_t = w[t]
        p_t = TL[t] * EF[t] * int(increase or -1)
        z_t = p_t + (1 - p_t) * (1 - w_t) * z_t
        z[t] = z_t

    y.values = ((1 - z) * y).astype(int)
    xr_imp["indicator"] = y
    xr_imp["indicator"] = xr_imp["indicator"].clip(min=0)

    try:
        if input_xarr["geometry"].any():
            output = xr.Dataset(
                {
                    "baseline": input_xarr["indicator"],
                    "intervention": xr_imp["indicator"],
                    "date": input_xarr["date"],
                    "geometry": input_xarr["geometry"],
                }
            )
    except KeyError:
        # mask over the negative values of y, to prevent warnings when plotting
        input_xarr["indicator"] = xr.DataArray(
            input_xarr["indicator"].where(
                input_xarr["indicator"] > -100.0,
            )
        )
        input_xarr["intervention"] = xr.DataArray(
            y.where(
                y > -100.0,
            )
        )
        output = input_xarr
    return output


def infection_intervention(initial_df, month_select, gdf, duration, q1, TE, mod_obj):
    """
    NOTE: need to take a closer look at this approach. It's finicky
    for measles and other auto-regressive model, this function uses
    the perturbed indicator value (e.g. post_interv_number) from the previous month
    to pass down as total_cases_lag1 for
    the next month.
    This function expects the following columns:"timestamp", "Admin_1", "post_interv_number"
    initial_df: the dataframe from month when first intervention took place
    month_select: list of months post FIRST intervention
    gdf: original dataframe (the indicator output from a model task)
    duration: if not default, must equal or be less than length of month_select
    q1: number of recipients
    TE: Target efficiency, how efficient is the delivery to the target recipients(default is 1)
    mod_obj: pretrained_model
    """
    accum_inter_df = [initial_df]

    if (duration - 1) == len(month_select):
        intervene_ls = [1] * len(month_select)
    elif (duration - 1) < len(month_select):
        non_interv = duration - 1
        intervene_ls = [1] * len(month_select)
        intervene_ls[non_interv:] = [0] * len(intervene_ls[non_interv:])

    for i in range(len(month_select)):

        post_interv_gdf = gdf.loc[gdf["timestamp"] == month_select[i]]
        # pull data from previous month
        prev_gdf = accum_inter_df[-1].copy()
        post_interv_gdf = post_interv_gdf.merge(
            prev_gdf[["admin1", "post_interv_number"]], on="admin1"
        )
        post_interv_gdf.loc[:, "total_cases_lag1"] = post_interv_gdf[
            "post_interv_number"
        ]
        # run model inference on the new laged cases from vaccine intervention
        feat_cols = ["total_cases_lag1", "log_pop", "num_clinics", "Month"]
        X_new = post_interv_gdf[feat_cols]
        X_new.loc[:, "num_clinics"] = X_new["num_clinics"].astype(int)
        prediction = mod_obj.predict(X_new)
        post_interv_gdf["post_interv_number"] = prediction.astype(int)

        # calculte new number of recipients for new round
        post_interv_gdf = calc_percentage(post_interv_gdf, "post_interv_number")
        post_interv_gdf.loc[:, "admin_recipients"] = (
            post_interv_gdf["case_percent"] * q1 * TE
        )
        post_interv_gdf.loc[:, "admin_recipients"] = post_interv_gdf[
            "admin_recipients"
        ].astype(int)
        post_interv_gdf.loc[:, "admin_recipients"][
            post_interv_gdf["admin_recipients"] > post_interv_gdf["post_interv_number"]
        ] = post_interv_gdf["post_interv_number"]
        post_interv_gdf.loc[:, "post_interv_number"] = (
            post_interv_gdf["post_interv_number"]
            - post_interv_gdf["admin_recipients"] * intervene_ls[i]
        )
        post_interv_gdf["post_interv_number"][
            post_interv_gdf["post_interv_number"] < 0
        ] = 0

        accum_inter_df.append(post_interv_gdf)

    accum_inter_df = pd.concat(accum_inter_df, axis=0)

    return accum_inter_df


class intervention_impact(object):
    def __init__(
        self,
        indicator_col,
        admin_level,
        provide_type,
        period_col,
        increase,
        TE,
        q1,
        q2,
        q3_norm,
        q3,
        alpha1,
        beta1,
        beta2,
        beta3,
        gamma2,
        gamma3,
        delta1,
        interv_window,
        reporting_dur,
    ):

        """Return an object for calculating impacted indictor values.
        source_fname: file path of the indicator data (string)
        admin_level: the admin level that it operates on
        period_col: the column or feature that corresponds to the time period
        TE: target efficiency
        EP: efficacy persistence  (fraction)
        EF: efficiency of the intervention to have intended impact (fraction)
        q1: number of recipients (integer)
        q2: frequency, or number of intervention events (integer). It needs to be less than reporting_dur
        q3_norm: normalized dose/treatment delivery efficiency
        q3: quantity of treatment/transaction
        report_dur: days, or time frame, of interest
        alpha1: placebo effect (e.g 0.1 for food, 0 for vaccine)
        beta3: Not to exceed 1
        gamma2: Each additional dose will have a small exponentially more powerful effect as nitrogen accumulates
        gamma3: No expectation of additional exponential effect with quantity of nitrogen, as it will run-off
        delta1: decay of the treatment over time
        interv_window: number of days between interventions
        See this README(WIP) for reference:
        https://gitlab.com/kimetrica/darpa/interventions/-/tree/master/interventions/impact
        """

        self.indicator_col = indicator_col
        self.admin_level = admin_level
        self.provide_type = provide_type
        self.period_col = period_col
        self.increase = increase
        self.TE = TE
        self.q1 = q1
        self.q2 = q2
        self.q3_norm = q3_norm
        self.q3 = q3
        self.alpha1 = alpha1
        self.beta1 = beta1
        self.beta2 = beta2
        self.beta3 = beta3
        self.gamma2 = gamma2
        self.gamma3 = gamma3
        self.delta1 = delta1
        self.interv_window = interv_window
        self.reporting_dur = reporting_dur

    def import_indicator(self, source_fname):
        """
        import indicator data (preferrably in geojson format) into geopandas dataframe,
        and calculate the TL (percent of recipients reached).
        Currently it only takes geojson or tiff files
        """
        ext = os.path.splitext(source_fname)[-1].lower()
        if ext == ".geojson":
            gdf = gpd.read_file(source_fname)
            gdf.drop_duplicates(
                subset=[self.admin_level, self.period_col], inplace=True
            )
            # this percentage calculation is only valid for geopanda DF disaggregated by admin level.
            indicator_data = calc_percentage(gdf, self.indicator_col)

        elif os.path.isdir(source_fname):
            if glob.glob(os.path.join(source_fname, "*.tiff")):
                ext = ".tiff"
                # sort the raster files by Time tag on the individual raster
                period_raster = {}
                for file in glob.glob(os.path.join(source_fname, "*.tiff")):
                    srcfile = rasterio.open(file)
                    tp = srcfile.tags()["Time"]
                    period_raster[tp.split("T")[0]] = file
                period_dict = dict(sorted(period_raster.items()))
                # Format raster array into xarray Dataset
                ds = []
                for k, v in period_dict.items():
                    tp1 = pd.to_datetime(k)
                    tp_1mo = tp1 + pd.DateOffset(months=1)
                    tp2 = pd.offsets.MonthEnd().rollback(tp_1mo)
                    # NOTE: after assigning new coordinate for 'period', expand the array with new dim
                    da = (
                        xr.open_rasterio(v, parse_coordinates=True, chunks={})
                        .sel(band=1)
                        .drop("band")
                        .assign_coords(period=tp1)
                        .expand_dims("period")
                    )
                    ds1 = da.to_dataset(name="indicator")

                    time_index2 = pd.date_range(start=tp1, end=tp2, freq="D")
                    # fill the day array with values of the date
                    ds.append(ds1.reindex({"period": time_index2}, method="ffill"))

                ds_concat = xr.concat(ds, "period").sortby("period")
                # replace the 0s with small number to prevent warnings about division by 0
                indicator_data = ds_concat.where(
                    ds_concat["indicator"].data != 0,
                    0.0001,
                )
                # enforce negative numbers to -9999.
                indicator_data = indicator_data.where(
                    indicator_data["indicator"].data > 0, -9999.0
                )

        else:
            print("wrong data format, returning an empty geopandas df")
            indicator_data = gpd.GeoDataFrame()
            ext = np.nan

        return indicator_data, ext

    def simulate_intervention(
        self,
        alpha1,
        beta1,
        beta2,
        beta3,
        delta1,
        gamma2,
        gamma3,
        q2,
        q3_norm,
        time,
        q3,
        reporting_duration,
    ):
        """"""
        step = 1  # Time step in days - default to one, TODO: test and ensure this is flexible, I think the for loop will need to be adjusted
        q2_current = 1  # Set the current distribution round to one
        if beta3 == 0:
            q3_norm = 1
            beta3 = 1
        ## Prepare simulation lists and dataframes
        dist_days = np.round(np.arange(0, time, time / q2))

        result_col = ["day", "effect", "dose day", "dose round"]
        result_df = pd.DataFrame(columns=result_col)

        ## Calculate the initial impact of the first round of distribution
        te_current = te_initial(alpha1, beta1, beta3, gamma3, q3_norm, q3)
        result_df = result_df.append(
            {
                "day": 0,
                "effect": te_current,
                "dose day": True,
                "dose round": q2_current,
            },
            ignore_index=True,
        )

        ## For each subsequent day, calculate the time-based degredation and for days that have a distribution, the impact of that distribution.
        for day in range(1, reporting_duration):
            if day in dist_days:
                q2_current = q2_current + 1

                round_improvement = te_round(
                    beta2, beta3, gamma2, gamma3, q2_current, q3_norm, te_current, q3
                )
                decay = te_decay(delta1, round_improvement, step)
                te_current = decay

                new_row = {
                    "day": day,
                    "effect": te_current,
                    "dose day": True,
                    "dose round": q2_current,
                }
            else:

                decay = te_decay(delta1, te_current, step)
                te_current = decay

                new_row = {
                    "day": day,
                    "effect": te_current,
                    "dose day": False,
                    "dose round": q2_current,
                }
            result_df = result_df.append(new_row, ignore_index=True)

        return result_df

    def indicator2Xr(self, gdf, ext):
        """
        converts geopandas to xarray ready for impact calculation.Calculate relevant EF.
        If provide type is vaccine, then skip this step
        ext: the file format (e.g., geojson, tiff, etc)
        """
        time = self.q2 * self.interv_window

        ef_df = self.simulate_intervention(
            self.alpha1,
            self.beta1,
            self.beta2,
            self.beta3,
            self.delta1,
            self.gamma2,
            self.gamma3,
            self.q2,
            self.q3_norm,
            time,
            self.q3,
            self.reporting_dur,
        )

        if (
            self.provide_type == "Free food distribution"
            or self.provide_type.lower() == "vaccine"
            or self.provide_type == "Supplementary feeding"
            or self.provide_type == "School feeding"
        ):
            if ext == ".geojson":

                if not gdf.empty:
                    gdf.loc[:, "indicator"] = gdf[self.indicator_col]
                    # this adds a "date" column
                    gdf_daily = daily_df(
                        gdf, period_col=self.period_col, admin_col=self.admin_level
                    )
                    gdf_df = day_assign(gdf_daily, admin_col=self.admin_level)
                    # merge with intervention EF data to create EF column

                    gdf_ef = gdf_df.merge(ef_df, on=["day"], how="left")
                    gdf_ef.dropna(subset=["effect"], inplace=True)
                    gdf_ef.loc[:, "EF"] = gdf_ef["effect"]
                    # use the date column, which was derived from self.period_col
                    gdf_select = gdf_ef[
                        [
                            "date",
                            "indicator",
                            "dose day",
                            "EF",
                            "geometry",
                            self.admin_level,
                        ]
                    ]
                    gdf_select.loc[:, "date"] = pd.to_datetime(gdf_select["date"])
                    # index will be used as coordinates for xarray, output shape should be (timestamp, location)
                    gdf_select = gdf_select.set_index(["date", self.admin_level])
                    xarr = gdf_select.to_xarray()

                    # for easier and general slicing, take only the value along time axis
                    xarr["EF"] = xarr["EF"][:, 0]
                    xarr["dose day"] = xarr["dose day"][:, 0]

            elif ext == ".tiff":
                if gdf.dims is not None:
                    ds_masked = gdf
                    ds_masked["dose day"] = xr.DataArray(ef_df["dose day"])
                    ds_masked["EF"] = xr.DataArray(ef_df["effect"])
                    # slice the dataset to match duration of intervention simulation
                    xarr = ds_masked.isel(period=slice(0, ef_df.shape[0]))

        elif self.provide_type == "other":
            if not gdf.empty:
                pass

        else:
            print("empty array")
            xarr = gdf.to_xarray()

        return xarr

    def indicator2df(self, gdf):
        """
        if provide type is for other type, rearrange the gdf for the initial month of intervention.
        This data will be passed on to the model to predict future cases of infections
        """
        if self.provide_type == "other":
            select_df = gdf.loc[gdf[self.period_col] == min(gdf[self.period_col])]

            # calculte number of recipients for each admin region based on percentage of cases

            select_df.loc[:, "admin_recipients"] = (
                select_df["case_percent"] * self.q1 * self.TE
            )
            select_df.loc[:, "admin_recipients"] = select_df["admin_recipients"].astype(
                int
            )
            select_df.loc[:, "admin_recipients"][
                select_df["admin_recipients"] > select_df[self.indicator_col]
            ] = select_df[self.indicator_col]
            select_df.loc[:, "post_interv_number"] = (
                select_df[self.indicator_col] - select_df["admin_recipients"]
            )
        else:
            pass
        return select_df

    def calc_impact(self, input_fname, trained_mod=None):
        gdf, ext = self.import_indicator(input_fname)
        if (
            self.provide_type == "Free food distribution"
            or self.provide_type.lower() == "vaccine"
            or self.provide_type == "Supplementary feeding"
            or self.provide_type == "School feeding"
        ):
            xarr = self.indicator2Xr(gdf, ext)
            if xarr.dims is not None:
                impact_output = distribution_intervention(
                    xarr, self.increase, self.q1, self.TE
                )

        elif self.provide_type == "other":
            # run model training task first for mod_obj input
            initial_df = self.indicator2df(gdf)
            month_ls = gdf[self.period_col].unique().tolist()
            month_ls.sort()
            month_select = month_ls[1:]
            intervention_output = infection_intervention(
                initial_df,
                month_select,
                gdf=gdf,
                duration=self.q2,
                q1=self.q1,
                TE=self.TE,
                mod_obj=trained_mod,
            )

            # turn dataframe to xarray
            output_reindx = intervention_output.set_index(
                [self.period_col, self.admin_level]
            )
            impact_output = output_reindx.to_xarray()

        else:
            pass
        return impact_output
