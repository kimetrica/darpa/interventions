The README.md description of the recursion equation has drifted from
the implementation. These coarse notes need to be cleaned up and written
into the README.

---

basic idea is:

"new_Y" is Y that has not previously been provisioned
"old_Y" is Y that has

Y_star_t = (1 - p) * new_Y + (1 - p) * f_q * old_Y
Y_star_t = (1 - p) * (new_Y + f_q * old_Y)

new_Y = w_t * Y_t
old_Y = (1 - w_t) * Y_t

f_q describes the persistence of impact on "old_Y" carrying over to this time period

f_0 * old_Y = old_Y
f_1 * old_Y = Y_star_{t-1} / Y_{t-1} * old_Y

f_q := ...

Y_star_t = (1 - p) * (new_Y + (1 - q) * old_Y + q * Y_star_{t-1} / Y_{t-1} * old_Y)

now if p = 0 and Y_star_{t-1} = Y_{t-1}, then
Y_star_t = new_Y + (1 - q) * old_Y + q * old_Y
Y_star_t = new_Y + old_Y

great!

subbing w_t

Y_star_t = (1 - p) * (w_t * Y_t + (1 - q) * (1 - w_t) * Y_t + q * Y_star_{t-1} / Y_{t-1} * (1 - w_t) * Y_t)
Y_star_t = (1 - p) * (w_t * Y_t + (1 - q * (1 - Y_star_{t-1} / Y_{t-1})) * (1 - w_t) * Y_t)

now a proportion as variable makes sense!

Y_star_t / Y_t = (1 - p) * (w_t + (1 - q * (1 - Y_star_{t-1} / Y_{t-1})) * (1 - w_t))
Z_t := 1 - Y_star_t / Y_t
Z_t = 1 - (1 - p) * (w_t + (1 - q * Z_{t-1}) * (1 - w_t))

Z_t = p + (1 - p) * (1 - w_t) * q * Z_{t-1}

now ... simple
if (no prior impact) Z_{t-1} = 0, then Z_t = p
if (all new) w_t = 1, then Z_t = p
if (no persistence) q = 0, then Z_t = p

... harder
if (all old) w_t = 0, then Z_t = p + (1 - p) * q * Z_{t-1} = p * (1 - q * Z_{t-1}) + q * Z_{t-1}
   (and with perfect persistence) q = 1, then Z_t = p * (1 - Z_{t-1}) + Z_{t-1}
   (so over time) Z_0 = 0, Z_1 = p, Z_2 = p * (2 - p)
