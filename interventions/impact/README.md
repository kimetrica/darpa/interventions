# Impact Models

Each class of "impact model" corresponds to one of the finite 
top-level types of interventions in the Kimetrica ontology. This README 
focuses on impact models for the provisioning type of intervention and gives
an example of provisioning a treatment to reduce malnutrition rates.

A principle guiding development of impact models is to only simulate a 
direct change to a proximate indicator (e.g output from the malnutrition model). Any response expected among other 
indicators should be derived from domain models. For this reason, the impact
model does not change the extent, resolution, or type of the input 
indicator, so the output is directly substitutable for further modeling.

## Parameters

#### Provide class

Each instance of an impact model has 3 scale parameters defined on a 
spatio-temporal interval (i.e. "area" and "period"). A provisioning "event"
is the distribution of some quantity of the provisioned object to a 
recipient at one time. For the malnutrition example (see below), an event 
may correspond to a person receiving a medical treatment (deworming). The 
following parameters capture the key quantities in a sentence like: "provide
 6 doses to school-age children on a monthly schedule".

| Parameter | Range | Unit/DataType | Description |
|---|---|---|---|
| $`q_1`$ | $`[0, \infty)`$ | number of people or household | coverage, or number of recipients |
| $`q_2`$ | $`[0, \infty)`$ | number of intervention rounds | frequency, or number of events |
| $`q_3`$  | $`[0, \infty)`$ | varies | quantity of treatment per recipient per intervention |
| G | | Polygon | geographic extent, indexed by $`g`$ |
| T | | DateInterval | temporal extent, indexed by $`t`$ |

Every impact model has additional parameters derived from the intervention 
type. These  should have accurate defaults (possibly stochastic), but would 
also be tunable by the user through creating a subclass of a particular 
intervention. "Target efficiency" refers to whether provisioned units 
make it to the recipient (not whether the units have the  intended impact, 
see $`E\!F`$).   

| Parameter | Range | Units | Description |
|---|---|---|---|
|$`T\!E`$ | $`(-1, 1)`$ | None | target efficiency coefficient |

Composite parameters determined by a function of input parameters that is
specific to each intervention. Here, $`E\!F`$ is calculated based on a recursive
equation that accounts for fixed impact, intrinsic dose effect, and the decay of the treatment 
efficiency through out time. More information can be found at this [link](https://kimetrica.atlassian.net/wiki/spaces/MOD/pages/178585607/Modeling+Overview) under Provider Class. 

| Parameter | Range | Input | Description |
|---|---|---|---|
|$`T\!L`$ | $`(0, 1)`$ | $`q_1, T\!E`$ | proportion of recipients provisioned |
|$`E\!F`$ | $`(0, 1)`$ | $`q_2, q_3 ,\alpha_1, \beta_1`$ <br> $`\beta_2, \gamma_2 ,\beta_3, \gamma_3, \delta`$ | treatment efficiency on indicator |

## Variables

The impact model takes an input indicator, which has an internally defined 
spatio-temporal extents and resolutions, and returns an output 
impacted indicator with the same metadata.

| Variable | Description |
|---|---|
| $`Y_{t,g}`$ | (input) indicator at time $`t`$ and place $`g`$ |
| $`Y^*_{t,g}`$ | (output) impacted indicator at time $`t`$ and place $`g`$ |

## Recursion Equation

For a scenario with a single timestep, the impact model is a proportional 
reduction (or increase, but only the reduction is shown here) in the indicator 
equal to the rate at which targets are reached times the efficacy rate of 
the provisioned service or commodity.

```math
\frac{Y_{g,t} - Y^*_{g,t}}{Y_{g,t}} = T\!L \times E\!F
```

Call the left-hand quantity $`Z_{g,t}`$, as this proportion turns out to
be the simpler quantity for carrying the intervention through multiple time 
steps that incorporate changes in the unperturbed indicator along with any 
persistence of impact between steps. Subscripts $`g`$ and $`t`$ on $`T\!L`$ 
and $`E\!F`$ are left off for clarity.

Define a weight $`w`$ for the proportion of $`Y_{g,t}`$ that was not previously 
exposed to the intervention, due to underlying change in the unperturbed 
indicator.

```math
w_{g,t} = \max \left\{0, \frac{Y_{g,t}-Y_{g,t-1}}{Y_{g,t}}\right\}
```

In the simple case where the impact never "wears off", the impact increases 
each time step, discounted by changes in the input indicator:

```math
Z_{g,t} = T\!L \times E\!F + (1 - T\!L \times E\!F) * (1 - w_{g,t}) * Z_{g,t-1}
```
The recursions is linear with time-varying coefficients, so a fine solution 
method is plain old iteration.


## Input variables

Within the Intervention model(`interventions.intervention_model`), there are two classes: `Intervention_provide` and `Intervention_build`. The former accepts `.geojson` and `.tiff` data format, while the latter accepts a shapefile data format. The input parameters for each class is discussed below.

#### Provide Class

For geojson data source, it needs to have a numeric column corresponding to `indicator_col` and a timestamp column corresponding to `period_col` in Intervention_provide(). The intervention model requires 2 sets of input parameters.

In the case of geotiff rasters, each raster should represent a time point and comes with a tag labeled `Time`.

* The Greek alphabets defined the intrinsic property of the intervention. These empirical values are based primarily on literature research.
    * alpha1: placebo effect (e.g 0.1 for food, 0 for vaccine)
    * beta1: impact for first round (between 0-1)
    * beta2: slope impact per increase in quantity of treatments(between 0-1)
    * beta3: slope impact per increase in an unit of treatment (not to exceed 1)
    * delta1: decay of the treatment efficiency over time (between 0-1)
    * gamma2: exponent decay per increase in quantity of treatments as time goes by (between 0-1)
    * gamma3: exponent decay of the increase per unit dose in time. (between 0-1)

**Note:** The Greek alphabets will soon have default values tied to different `provide_type`.  

* The other parameters are related to the scale of the intervention
    * q1: number of recipients per round of intervention
    * q2: number of intervention events (integer). It needs to correspond to reporting_dur
    * q3: quantity of treatment/transaction for one round of intervention
    * q3_1 to q3_4: the suffix refer to the different items in one unit of treament for one round of intervention (this is for estimating the total cost)
    * q3_norm: normalized dose/treatment delivery efficiency
    * interv_window: frequency of the intervention (e.g. every 7 days)
    * reporting_dur: total duration of the intervention period (approximately q2 * interv_window)

#### Build Class

The `Build` intervention class runs scenarios for either road upgrading or road extension. It requires an input of a shapefile with road condition attributes (e.g, very poor, poor, fair, good, excellent), and the user specifies a coordinate point and type of intervention ("upgrade" or "add link").

* provide_type: the type of intervention. For instance, "upgrade" means repair or upgrade of roads in poor or very poor condition
"add link" means adding new roads.
* road_shapefile: input geopandas dataframe where the geometry is a LineString type
* coords: specifed as a list like [( (x_0, y_0),(x_1, y_1) )], where x=longitude and y= latitude. It can be just one point but generalizable to multistring points like [(x,y)]. Note that the first tuple should (x_0, y_0) be the starting point.

If the `provide_type` is "upgrade" then the user will also need to specify the target condition (string value). For the 
`provide_type` of "add link", the `coords` would need to be at least two points (see example below). 

## Using the model

#### Provide class 

The example below shows how to call the `Intervention_provide` function for food distribution (same input parameters for geojson and geotiff)

```python
from interventions.intervention_model import Intervention_provide
maln_target='s3://darpa-output-dev/final_targets/malnutrition_740cd03035.geojson'
cost_fname='https://darpa-output-dev.s3.amazonaws.com/final_targets/cost1_reg_output_4c717b2e0d.pickle'

free_food=Intervention_provide(indicator_col='gam_number',
                             admin_level='admin2',
                             provide_type='Free food distribution',
                             period_col='start',alpha1=0.1, 
                             beta1=0.91, beta2=0.2, beta3=0.8, 
                             delta1=0.1,gamma2=0.7, gamma3=0.2,)

impact_effect, impact_cost=free_food.impact_calculation(source_fname=maln_target,
                                           q1=1000, q2=4*4,q3=1,
                                           q3_1=2,q3_2=0, 
                                           q3_3=0, q3_4=0,
                                           q3_norm=1,
                                           interv_window=7,
                                           reporting_dur=30*4,
                                           model_fname=cost_fname)

```
In the case of measles vaccine distribution, the user needs to change the `provide_type` parameter as well as the greek alphabets 
as shown in the example below

```python
measles_intervention=Intervention_provide(indicator_col='measles_number',
                                        provide_type='vaccine',
                                        admin_level='admin1',
                                        period_col='timestamp',
                                        alpha1=0.0, beta1=0.91, beta2=0.03, beta3=0, 
                                         delta1=0.0,)
```
#### Build Class

For the scenario of adding new roads to existing road, the `Intervention_build` class returns the length of road affected.


```python
from interventions.intervention_model import Intervention_build
origin = (38.088, 7.970)
destination = (38.095, 8.01)
coords1 = [(origin, destination)]
build=Intervention_build(roads_line, coords1, provide_type='add link')
build_length=build.impact_length()
print(build_length)
```
An example of road upgrade intervention is shown below, where the function also returns the scope of the upgrade.

```python
origin = (38.088, 7.970)
build=Intervention_build(roads_line,[origin], provide_type='upgrade')
build_length=build.impact_length()
upgrade_range=build.quality_upgrade('excellent')
print(build_length)
print('range of upgrade:', upgrade_range)
```

## Outputs of the model

The output format of the model will be the same as the input format, with the addition of the indicator values post intervention.
For geojson output, there will be an additional feature called `intervention`. Similarly for raster output there will be an 
additional variable called `intervention` for the xarray dataset.

For geojson input file with data on the administrative level, try plotting with `hvplot` to get an interactive map.

```python
baseline=impact_effect["baseline"].hvplot(groupby="admin2", label='baseline')
intervention=impact_effect["intervention"].hvplot(groupby="admin2", label='intervention')
(baseline * intervention).opts(width=600, legend_position='top_right')
```
The rasterized xarray result can be visualized with `xr.plot.pcolormesh`. 

```python
#  slice the xarray every 7 day for visualization
result_7d=impact_effect.isel(period=slice(0, impact_effect.dims['period'], 7))

# show the difference as raster plot
xr.plot.pcolormesh(np.log(result_7d[['indicator'][1]-result_7d[['intervention'][1]+1),
cmap='pink')
```

## Constraints

For the `Provide` class, there's currently two provide types available: `Free food distribution` and `vaccine`, more options will be added soon.




