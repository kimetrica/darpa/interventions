import numpy as np
import pandas as pd
import requests
import statsmodels.api as sm
import statsmodels.formula.api as smf  # noqa: F401
from luigi.configuration import get_config
from luigi.util import requires

from kiluigi.targets import FinalTarget, IntermediateTarget
from kiluigi.targets.ckan import CkanTarget
from kiluigi.tasks import ExternalTask, ReadDataFrameTask, Task

# Packages for importing directly from the spreadsheet
config = get_config()


class PullTrainingData(ExternalTask):
    """
    Access dataset on Kimetrica's Ckan (https://data.kimetrica.com)
    """

    # This version is in case we want to use data from Ckan

    def output(self):
        return {
            "InterventionTimelines": CkanTarget(  # CKAN Address for KimetricaInterventionContent Spreadsheet
                dataset={"id": "8fa9683c-3161-4143-a879-bdbb95b6941a"},
                resource={"id": "ac337104-4571-4ad8-bcaa-ea4bd1168a85"},
            ),
        }


@requires(PullTrainingData)
class ReadProvideTrainingData(ReadDataFrameTask):

    read_method = "read_excel"
    read_args = {"sheet_name": "InterventionTimelinesUpdated"}

    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 7)


class ReadFromApi(ExternalTask):
    """
    Read data from KDWdev Api
    """

    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 7)

    def run(self):

        parameters = {"page_size": 900}
        authentications = (config.get("kdw", "username"), config.get("kdw", "password"))

        response = requests.get(
            "https://kdwdev.kimetrica.com/api/provide_interventionsvalue/?format=json",
            params=parameters,
            auth=authentications,
        )

        r_json = response.json()

        # r_json nested JSON, use the Pandas built-in json_normalize() function to flatten ata to df
        inter_df = pd.json_normalize(r_json, record_path=["results"])
        inter_df = inter_df.rename(columns={"value": "total_cost_USD"})

        inter_df = inter_df[
            [
                "intervention_type",
                "intervention_location",
                "phase_start_date",
                "phase_end_date",
                "number_of_recipients",
                "number_of_rounds",
                "item_name_1",
                "unit_of_measure_1",
                "unit_per_recipient_per_round_1",
                "item_name_2",
                "unit_of_measure_2",
                "unit_per_recipient_per_round_2",
                "item_name_3",
                "unit_of_measure_3",
                "unit_per_recipient_per_round_3",
                "item_name_4",
                "unit_of_measure_4",
                "unit_per_recipient_per_round_4",
                "total_cost_USD",
                "overhead_cost_USD",
            ]
        ]

        inter_df[
            [
                "number_of_recipients",
                "number_of_rounds",
                "unit_per_recipient_per_round_1",
                "unit_per_recipient_per_round_2",
                "unit_per_recipient_per_round_3",
                "unit_per_recipient_per_round_4",
                "total_cost_USD",
                "overhead_cost_USD",
            ]
        ] = inter_df[
            [
                "number_of_recipients",
                "number_of_rounds",
                "unit_per_recipient_per_round_1",
                "unit_per_recipient_per_round_2",
                "unit_per_recipient_per_round_3",
                "unit_per_recipient_per_round_4",
                "total_cost_USD",
                "overhead_cost_USD",
            ]
        ].apply(
            pd.to_numeric, errors="coerce"
        )

        # Create the intervention year
        inter_df["year"] = pd.DatetimeIndex(inter_df["phase_end_date"]).year
        inter_df["direct_cost_USD"] = (
            inter_df["total_cost_USD"] - inter_df["overhead_cost_USD"]
        )

        unique_types = (inter_df["intervention_type"]).unique()
        unique_intervention = pd.DataFrame(
            data=unique_types, columns=["intervention_type"]
        )
        unique_intervention.dropna(inplace=True)

        inter_dict = {
            "intervention_df": inter_df,
            "intervention_type": unique_intervention,
        }

        with self.output().open("w") as output:
            output.write(inter_dict)


@requires(ReadProvideTrainingData)
class ProvideDatatoDF(Task):
    """
    Old Version of the module: Reading data from CKAN
    """

    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 7)

    def run(self):
        with self.input().open("r") as src:
            intervention_data = src.read()

        inter_df = intervention_data["InterventionTimelines"]

        # Selecting relevant columns
        inter_df = inter_df[
            [
                "intervention_type",
                "intervention_location",
                "phase_start_date",
                "phase_end_date",
                "number_of_recipients",
                "number_of_rounds",
                "item_name_1",
                "unit_of_measure_1",
                "unit_per_recipient_per_round_1",
                "item_name_2",
                "unit_of_measure_2",
                "unit_per_recipient_per_round_2",
                "item_name_3",
                "unit_of_measure_3",
                "unit_per_recipient_per_round_3",
                "item_name_4",
                "unit_of_measure_4",
                "unit_per_recipient_per_round_4",
                "total_cost_USD",
                "overhead_cost_USD",
            ]
        ]

        # Create the intervention year
        inter_df["year"] = pd.DatetimeIndex(inter_df["phase_end_date"]).year
        inter_df["direct_cost_USD"] = (
            inter_df["total_cost_USD"] - inter_df["overhead_cost_USD"]
        )

        unique_types = (inter_df["intervention_type"]).unique()
        unique_intervention = pd.DataFrame(
            data=unique_types, columns=["intervention_type"]
        )
        unique_intervention.dropna(inplace=True)

        inter_dict = {
            "intervention_df": inter_df,
            "intervention_type": unique_intervention,
        }

        with self.output().open("w") as output:
            output.write(inter_dict)


@requires(ReadFromApi)
class GeneralRegression(Task):
    def output(self):
        return FinalTarget(
            path="cost_2_reg_output.pickle", task=self, ACL="public-read"
        )

        # return IntermediateTarget(task=self, timeout=3600)

    def run(self):

        with self.input().open("r") as src:
            inter_df = src.read()["intervention_df"]

        with self.input().open("r") as src:
            intervention = src.read()["intervention_type"]
            inter_types = intervention["intervention_type"].tolist()

        # all_interv_regression=pd.DataFrame(columns=["Coef Estimates", "[0.025", "0.975]", 'pvalues', 't'])
        all_interv_regression_dict = {}

        for i in inter_types:
            inter_specific = inter_df.loc[inter_df["intervention_type"] == i]
            inter_specific = inter_specific.dropna(subset=["number_of_recipients"])
            inter_specific = inter_specific.dropna(
                subset=["unit_per_recipient_per_round_1"]
            )

            all_x_y = inter_specific
            # Read q1, q2
            q1_q2 = inter_specific[["number_of_recipients", "number_of_rounds"]]

            # Read all q3's
            all_q = inter_specific.columns
            q3 = [q for q in all_q if q.startswith("unit_per_recipient_per_round_")]
            all_q3 = inter_specific[q3]

            n = q1_q2.count()
            n_obs = n[1]

            if n_obs > 10:

                # err_message = "You can proceed with a regression model"
                # error_n = 0

                # Read c
                c = all_x_y["direct_cost_USD"]
                lnc = np.log(c)

                lnq1 = np.log(q1_q2["number_of_recipients"])
                lnq2 = np.log(q1_q2["number_of_rounds"])

                all_q3 = all_q3.dropna(axis=1, how="all")
                all_q3 = all_q3.fillna(1)
                # print(all_q3)
                all_q3 = all_q3 * (0.001)
                # print(all_q3)
                lnq3 = np.log(all_q3)

                # year_dummy = pd.get_dummies(all_x_y["year"]) # If we have a large dataset then we can use this

                Q = np.column_stack((lnq1, lnq2, lnq3))
                Q = sm.add_constant(Q)

                model = sm.OLS(lnc, Q, missing="drop")
                results = model.fit()

                # lnypred = results.predict(Q) # Why do we have this module?
                # pred_cost = np.exp(lnypred)
                # cost_flat = pred_cost.flatten()
                # cost = np.array([cost_flat])
                # cost_df = pd.DataFrame(data=cost, index=["predicted"])
                # predicted = cost_df.transpose()

                df = results.conf_int()
                df["Coef Estimates"] = results.params
                df["pvalues"] = results.pvalues
                df["t"] = results.tvalues
                df = df.rename(columns={0: "[0.025", 1: "0.975]"})
                col_name = "Coef Estimates"
                first_col = df.pop(col_name)
                df.insert(0, col_name, first_col)

                all_interv_regression_dict[i] = df

        with self.output().open("w") as output:
            output.write(all_interv_regression_dict)
