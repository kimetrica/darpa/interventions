import os.path

# Packages for importing directly from the spreadsheet
import pickle

import geocoder
import luigi
import numpy as np
import pandas as pd
import statsmodels.api as sm
from geopy.distance import distance
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from kiluigi.targets import FinalTarget, IntermediateTarget
from kiluigi.targets.ckan import CkanTarget
from kiluigi.tasks import ExternalTask, Task
from luigi.util import requires  # noqa: F401
from osgeo import gdal
from rasterstats import zonal_stats
from scipy.optimize import nnls
from shapely.geometry import Point
from sklearn.compose import ColumnTransformer
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OrdinalEncoder, StandardScaler

from .mapping import material_map
from .utils import summary_stats


class PullTrainingData(ExternalTask):
    """
    Access dataset on Kimetrica's Ckan (https://data.kimetrica.com)
    """

    def output(self):
        return {
            "InterventionTimelines": CkanTarget(
                dataset={"id": "8fa9683c-3161-4143-a879-bdbb95b6941a"},
                resource={"id": "c898b554-9549-4a94-9328-7948446ab68a"},
            ),
            "cred": CkanTarget(
                dataset={"id": "8fa9683c-3161-4143-a879-bdbb95b6941a"},
                resource={"id": "54954ffb-cc61-4eb1-a18c-283ce4219178"},
            ),
            "tok": CkanTarget(
                dataset={"id": "8fa9683c-3161-4143-a879-bdbb95b6941a"},
                resource={"id": "7d74391b-29f0-47e7-99fd-41e18f629366"},
            ),
        }


class PullPossiblePredictors(ExternalTask):
    def output(self):
        return {
            "dem": CkanTarget(
                dataset={"id": "f25f2152-e999-415f-956f-3a7c2eb2b723"},
                resource={"id": "8cdbc26c-cf9f-4107-a405-d4e4e1777631"},
            ),
            "night_time_light": CkanTarget(
                dataset={"id": "19aa8884-ed48-4c25-88e9-c1f96d5877bc"},
                resource={"id": "b3f20a35-0351-4ee1-a8d4-793e70910f1a"},
            ),
        }


@requires(PullPossiblePredictors)
class CalculateSlope(Task):
    def output(self):
        return IntermediateTarget(path="slop.tif", task=self, timeout=60 * 60 * 24 * 7)

    def run(self):
        src_file = self.input()["dem"].path
        with self.output().open("w") as out:
            gdal.DEMProcessing(out.name, src_file, "slope", scale=111120)


class PullTargetVariable(Task):
    def output(self):
        return CkanTarget(
            dataset={"id": "8fa9683c-3161-4143-a879-bdbb95b6941a"},
            resource={"id": "301523ce-fd41-4532-b1af-e3ae29e0b43f"},
        )


@requires(PullTargetVariable)
class ReadTargetVariable(Task):
    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 7)

    @staticmethod
    def geocode(i):
        res = geocoder.osm(i)
        if not res.latlng:
            return tuple([np.nan, np.nan])
        else:
            return tuple(res.latlng)

    @staticmethod
    def get_lat_lng(row, lat_lng_map, origin=True):
        if origin:
            names = ",".join(row[["origin", "origin_country"]])
        else:
            names = ",".join(row[["destination", "destination_country"]])
        return lat_lng_map.get(names)

    def run(self):
        df = pd.read_excel(self.input().path, sheet_name="data_clean")
        df.columns = [i.strip() for i in df.columns]
        for var in ["origin_country", "origin", "destination_country", "destination"]:
            df[var] = df[var].str.strip()

        df = df.dropna(
            subset=[
                "origin_country",
                "origin",
                "destination_country",
                "destination",
                "actual_cost (USD)",
            ]
        )
        df = df.reset_index()
        origin_set = set(
            df[["origin", "origin_country"]].apply(lambda x: ",".join(x), axis=1)
        )
        dest_set = set(
            df[["destination", "destination_country"]].apply(
                lambda x: ",".join(x), axis=1
            )
        )
        o_d_set = origin_set.union(dest_set)
        lat_lng_map = {}

        for i in o_d_set:
            lat_lng_map[i] = self.geocode(i)
        # Manual work
        lat_lng_map["Globe cinema,Kenya"] = (-1.2794566, 36.8211233)
        lat_lng_map["Ageremariam,Ethiopia"] = (5.6365707, 38.2219504)
        lat_lng_map["Kagitumba,Rwanda"] = (-1.0574598, 30.4544449)
        lat_lng_map["Chida Shawura,Ethiopia"] = (7.150258, 36.7828059)
        lat_lng_map["Gisiza,Rwanda"] = (
            -1.9178954,
            30.0539304,
        )

        df["origin_latlng"] = df.apply(
            lambda row: self.get_lat_lng(row, lat_lng_map, True), axis=1
        )
        df["destination_latlng"] = df.apply(
            lambda row: self.get_lat_lng(row, lat_lng_map, False), axis=1
        )

        with self.output().open("w") as out:
            out.write(df)


@requires(ReadTargetVariable, CalculateSlope, PullPossiblePredictors)
class GetRasterstats(Task):
    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 7)

    def run(self):
        with self.input()[0].open() as src:
            df = src.read()
        slope_src = self.input()[1].path
        dem_src = self.input()[2]["dem"].path
        ntl_src = self.input()[2]["night_time_light"].path
        df = summary_stats(df, slope_src, "origin_latlng", "origin_slope")
        df = summary_stats(df, slope_src, "destination_latlng", "dest_slope")

        df = summary_stats(df, dem_src, "origin_latlng", "origin_dem")
        df = summary_stats(df, dem_src, "destination_latlng", "dest_dem")

        df = summary_stats(df, ntl_src, "origin_latlng", "origin_ntl")
        df = summary_stats(df, ntl_src, "destination_latlng", "dest_ntl")
        with self.output().open("w") as out:
            out.write(df)


@requires(GetRasterstats)
class Train(Task):
    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 7)

    def run(self):
        with self.input().open() as src:
            df = src.read()
        df["Approval date (MM/DD/YY)"] = pd.to_datetime(df["Approval date (MM/DD/YY)"])
        df["aprroval_year"] = df["Approval date (MM/DD/YY)"].dt.year
        df["Planned completion data (MM/DD/YY)"] = pd.to_datetime(
            df["Planned completion data (MM/DD/YY)"]
        )
        df["completion_year"] = df["Planned completion data (MM/DD/YY)"].dt.year
        num_preds = [
            "dest_ntl_mean",
            "origin_ntl_mean",
            "origin_dem_std",
            "length",
            "aprroval_year",
        ]
        df["link_material_after"] = df["link_material_after"].replace(material_map)
        df["link_material_before"] = df["link_material_before"].replace(material_map)
        df["link_material_before"] = df["link_material_before"].fillna("Add link")
        ordinal_preds = ["link_material_before"]
        df = df.dropna(subset=num_preds + ordinal_preds + ["actual_cost (USD)"])
        X = df[num_preds + ordinal_preds]
        y = df["actual_cost (USD)"]
        preprocessor = ColumnTransformer(
            [
                ("numeric", StandardScaler(), num_preds),
                ("ordered", OrdinalEncoder(), ordinal_preds),
            ]
        )

        pipeline = Pipeline(
            [("preprocessor", preprocessor), ("estimator", LinearRegression())]
        )

        pipeline.fit(X, y)
        with self.output().open("w") as out:
            out.write(pipeline)


@requires(Train, PullPossiblePredictors)
class Predict(Task):
    year = luigi.IntParameter(default=2018)
    origin_lnglat = luigi.TupleParameter(default=(39.667169, -4.05052))
    destination_lnglat = luigi.TupleParameter(default=(39.67507159193717, -3.15073925))
    current_link_material = luigi.ChoiceParameter(
        default="gravel", choices=["gravel", "Add link", "bitumen"]
    )

    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 7)

    def run(self):
        with self.input()[0].open() as src:
            pipeline = src.read()

        df = self.inference_data()
        df["link_material_before"] = self.current_link_material
        df["cost"] = pipeline.predict(df)
        with self.output().open("w") as out:
            out.write(df)

    def inference_data(self):
        origin = Point(self.origin_lnglat)
        origin_buffer = origin.buffer(1)
        dest = Point(self.destination_lnglat)
        dest_buffer = dest.buffer(1)

        ntl_src = self.input()[1]["night_time_light"].path
        dem_src = self.input()[1]["dem"].path

        stats = {}
        stats["dest_ntl_mean"] = zonal_stats(dest_buffer, ntl_src, stats="mean")[0][
            "mean"
        ]
        stats["origin_ntl_mean"] = zonal_stats(origin_buffer, ntl_src, stats="mean")[0][
            "mean"
        ]
        stats["origin_dem_std"] = zonal_stats(origin_buffer, dem_src, stats="std")[0][
            "std"
        ]
        stats["length"] = distance(self.origin_lnglat, self.destination_lnglat).km
        stats["aprroval_year"] = self.year
        df = pd.DataFrame([stats])
        return df


@requires(PullTrainingData)
class PullDataSpreadsheet(ExternalTask):

    """
    Pulling data from KimetricaInterventionContent spreadsheet
    """

    def get_existing_interventions(self, sht, sample_range):

        SAMPLE_SPREADSHEET_ID = "1Ie0v2wWbnOBqAbRA-S1whg0YyI_T67jfvK66r5sG1UM"

        result = (
            sht.values()
            .get(spreadsheetId=SAMPLE_SPREADSHEET_ID, range=sample_range)
            .execute()
        )
        intervention_list = result.get("values", [])
        intervention_df = pd.DataFrame(
            intervention_list[1:], columns=intervention_list[0]
        )
        return intervention_df

    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 7)

    def run(self):

        path_name = self.input()

        creds_path = path_name["cred"].path
        token_path = path_name["tok"].path

        SCOPES = [
            "https://www.googleapis.com/auth/spreadsheets",
            "https://www.googleapis.com/auth/drive.file",
            "https://www.googleapis.com/auth/drive",
        ]

        if os.path.exists(token_path):
            with open(token_path, "rb") as token:
                creds = pickle.load(token)
                # If there are no (valid) credentials available, let the user log in.

        # creds = None # Use this line in case there is problems with credntials
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:

                flow = InstalledAppFlow.from_client_secrets_file(creds_path, SCOPES)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open(token_path, "wb") as token:
                pickle.dump(creds, token)

        service = build("sheets", "v4", credentials=creds)

        # Call the Sheets API
        sheet = service.spreadsheets()

        # read existing google sheet data
        lst_existing_intervention_names = self.get_existing_interventions(
            sheet, "Copy-of-BuildLinkRoadDataExtraction"
        )

        data_intervention_name = lst_existing_intervention_names[
            [
                "Project Code (IATI identifier)",
                "Phase Start Date",
                "Phase End Date",
                "Road_improvement",
                "length",
                "width",
                "actual_cost_total",
            ]
        ]

        with self.output().open("w") as output:
            output.write(data_intervention_name)


@requires(PullDataSpreadsheet)
class BuildDataPrep(Task):
    """
    Cleaning and Processing data
    """

    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 7)

    def run(self):
        with self.input().open("r") as src:
            build_df = src.read()

        build_df["Road_improvement"] = pd.to_numeric(build_df["Road_improvement"])
        build_df["actual_cost_total"] = pd.to_numeric(build_df["actual_cost_total"])
        build_df["length"] = pd.to_numeric(build_df["length"])

        build_df["Phase Start Date"] = pd.to_datetime(
            build_df["Phase Start Date"], format="%Y-%m-%d"
        )
        build_df["Phase End Date"] = pd.to_datetime(
            build_df["Phase End Date"], format="%Y-%m-%d"
        )

        build_df["duration"] = build_df["Phase End Date"] - build_df["Phase Start Date"]

        build_df = build_df.dropna(
            subset=["Road_improvement", "length", "actual_cost_total"]
        )

        build_df = build_df[build_df["length"] < 1000]
        build_df = build_df[build_df["length"] > 23]

        with self.output().open("w") as output:
            output.write(build_df)


@requires(BuildDataPrep)
class BuildRegression(Task):
    def output(self):
        file_path = f'{"cost_intervention/coefficients_estimate_2021_01_13"}'
        try:
            return FinalTarget(file_path, task=self, ACL="public-read")
        except TypeError:
            return FinalTarget(file_path, task=self)

    def run(self):
        with self.input().open("r") as src:
            build_prep = src.read()

        q1 = build_prep["length"].to_numpy()
        q2 = build_prep["Road_improvement"].to_numpy()
        Q = np.column_stack((q1, q2))
        Q = sm.add_constant(Q)
        y = build_prep["actual_cost_total"].to_numpy()

        results_nnl = nnls(Q, y)

        with self.output().open("w") as output:
            output.write(results_nnl)
