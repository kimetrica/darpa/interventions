import geopandas as gpd
from rasterstats import zonal_stats
from shapely.geometry import Point


def summary_stats(df, src_file, var, var_name):
    gdf = gpd.GeoDataFrame(df, geometry=[Point(i[1], i[0]) for i in df[var]])
    gdf["geometry"] = gdf.geometry.buffer(0.1)
    stat = ["median", "max", "mean", "min", "std"]
    stats = zonal_stats(gdf, src_file, stats=stat, geojson_out=True)
    df = gpd.GeoDataFrame.from_features(stats)
    df = df.rename(columns={i: f"{var_name}_{i}" for i in stat})
    df = df.drop("geometry", 1)
    return df
