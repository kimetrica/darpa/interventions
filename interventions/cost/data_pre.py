from kiluigi.tasks import Task, ExternalTask, ReadDataFrameTask
from kiluigi.targets import CkanTarget, IntermediateTarget
from luigi.util import inherits, requires
import numpy as np

from ..util.config import ProvideType
from ..util.tasks import AugmentIndicatorDataset


class PullTrainingData(ExternalTask):
    """
    Access dataset on Kimetrica's Ckan (https://data.kimetrica.com)
    """

    # TODO: training data should come from intervention API, not Ckan

    def output(self):
        return {
            "InterventionTimelines": CkanTarget(
                dataset={"id": "8fa9683c-3161-4143-a879-bdbb95b6941a"},
                resource={"id": "6f90cf2a-700a-4579-a7eb-b2c9d524b2d1"},
            )
        }


@requires(PullTrainingData)
class ReadProvideTrainingData(ReadDataFrameTask):

    read_method = "read_excel"
    read_args = {"sheet_name": "InterventionTimelinesUpdated"}

    def output(self):
        return IntermediateTarget(task=self)


@requires(AugmentIndicatorDataset)
@inherits(ProvideType)
class MakeProvidePredictionData(Task):
    """
    Derive the independent variables for the cost model from the user specified
    intervention parameters.
    """

    # TODO: there does not appear to be a independent variable in the cost
    #       model corresponding to all provide scale parameters?

    def output(self):
        return IntermediateTarget(task=self)

    def run(self):

        with self.input().open("r") as f:
            ds = f.read()

        # prepare for calculations on units per recipient
        s = np.array(self.number_of_recipients)
        t = np.array(self.number_of_rounds)

        # distribute recipients across locations
        # TODO: provide options other than uniform
        p = ds["area"] / ds["area"].sum()
        s = s * p

        # distribute rounds across periods
        # TODO: provide options other than uniform
        p = ds["duration"] / ds["duration"].sum()
        t = p * t

        # determine the number of provisioning sites from the combination of
        # spatial resolution and extent
        if "area" in ds:
            ds = ds.assign(
                {
                    "number_of_recipients": (
                        np.array(self.number_of_recipients)
                        * (ds["area"] / ds["area"].sum())
                    ),
                }
            )
        else:
            # TODO: gridded data
            raise NotImplementedError

        # stack by converting to DataFrame
        df = ds.to_dataframe()

        with self.output().open("w") as f:
            f.write(df)
