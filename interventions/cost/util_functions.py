import numpy as np


class Intervention_cost(object):
    def __init__(self, mod_obj, interv_type):

        self.interv_type = interv_type
        self.mod_obj = mod_obj

    def provide_cost_calculation(
        self, q1, q2, q3, q3_1, q3_2=None, q3_3=None, q3_4=None
    ):
        """
        This function calculates the cost of intervention
        mod_obj: loaded instance of model
        interv_type: intervention type (e.g "Free food distribution", "vaccine")
        q1: number of recipients,
        q2: number of intervention rounds
        q3: number of unit/bundle per recipent per round of intervention
        q3_1: quantity of good1 in one unit of distribution
        q3_2: quantity of good2 in one unit of distribution
        q3_3: quantity of good3 in one unit of distribution
        q4_4: quantity of good4 in one unit of distribution
        some quantities might be zero/None, depending on the intervention type sometimes only
        one or two quantities are needed
        """

        cash_program = ["Cash transfer (Unconditional)"]
        food_program = [
            "Free food distribution",
            "Supplementary feeding",
            "School feeding",
        ]
        provide_mod = self.mod_obj

        if self.interv_type != "vaccine" and self.interv_type in food_program:
            df = provide_mod[self.interv_type]
            alpha = df["Coef Estimates"][0]
            beta_1 = df["Coef Estimates"][1]
            beta_2 = df["Coef Estimates"][2]

            if self.interv_type in food_program:
                item_ls = [q3_1, q3_2, q3_3, q3_4]
            elif self.interv_type in cash_program:
                item_ls = [q3_1]
            else:
                item_ls = [q3_1]
            # replace None value to zero (q3_1 cannot have zero value)
            item_ls = [0 if v is None else v for v in item_ls]

            beta3_q_3 = 0
            for i in range(len(item_ls)):
                if item_ls[i] & int(item_ls[i]) > 0:
                    beta3_q_3 = (
                        df["Coef Estimates"][i + 3] * np.log(float(item_ls[i] * q3))
                        + beta3_q_3
                    )
                else:
                    beta3_q_3 = df["Coef Estimates"][i + 3] * np.log(1) + beta3_q_3

            ln_calc_cost = (
                alpha
                + beta_1 * np.log(float(q1))
                + beta_2 * np.log(float(q2))
                + beta3_q_3
            )
            calc_cost = np.exp(ln_calc_cost)

        else:
            # for measeles vaccine, the average cost per dose is $1.73
            calc_cost = q1 * q2 * q3 * 1.73

        return calc_cost

    def build_cost_calculation(self, length, target_condition):
        """
        This function calculates the cost of intervention
        mod_obj: loaded instance of model
        length: length of roads,

        """

        build_program = ["add link", "upgrade"]

        if self.interv_type in build_program:
            build_mod = self.mod_obj
            build_cost = (
                build_mod.params[0]
                + build_mod.params[1] * length
                + build_mod.params[2] * target_condition
            )
        else:
            pass
        return build_cost
