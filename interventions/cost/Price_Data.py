#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 17:54:51 2020
This code is a WORK IN PROGRESS

@author: ahmadmohassel
"""

from bs4 import BeautifulSoup  # noqa: F401
import pandas as pd
import requests  # noqa: F401

from luigi.util import requires
import urllib.request  # noqa: F401
from kiluigi.targets import CkanTarget, IntermediateTarget
from kiluigi.tasks import ExternalTask, Task


class TabularFilesCKAN(ExternalTask):
    def output(self):
        return {
            "Global_Price": CkanTarget(
                resource={"id": "ff5e7a7c-1da7-4316-b772-f17784d18e32"},
            ),
        }


@requires(TabularFilesCKAN)
class ReadData(Task):
    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 30)

    def run(self):

        price_data = self.input()["Global_Price"]

        global_price_data = pd.read_excel(price_data.path)
        print(type(global_price_data))

        with self.output().open("w") as output:
            output.write(global_price_data)


# TODO : Write the module for filtering cereals:
@requires(ReadData)
class PrepareCommodity(Task):
    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 30)

    def run(self):

        with self.input().open("r") as f:
            price_data = f.read()

        # unique_commodities = (price_data["Items"]).unique()
        # unique_commodity = pd.DataFrame(data=unique_commodities, columns=["Items"])
        # unique_commodities.dropna(inplace=True)

        cereal_group = [
            "Sorghum",
            "Wheat",
            "Oats",
            "Bread Wheat",
            "Durum Wheat",
            "Maize",
            "Malting Barley",
            "Barley",
            "Rye",
            "Bread Rye",
            "Rice",
            "Sorghum",
            "Maize (corn)",
            "Millet",
            "Wheat grain",
            "What Flour",
            "Bread",
            "Cassava Flour",
            "Millet Flour",
            "Milled rice",
        ]

        # "Spaghetti", "Maize Tortilla", "Maize Meal", "Roller Maize Meal"

        price_cereals = price_data[price_data.Items.isin(cereal_group).astype(int) == 1]
        price_cereals = price_cereals[
            price_cereals["Standard Unit of Measurement"] == "USD/tonne"
        ]

        with self.output().open("w") as output:
            output.write(price_cereals)
