from pathlib import Path
import json

from sklearn.linear_model import LinearRegression as Estimator
from sklearn.compose import ColumnTransformer, TransformedTargetRegressor
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import PowerTransformer
from sklearn.pipeline import Pipeline
from luigi.util import inherits, requires
from kiluigi import (
    Task,
    IntermediateTarget,
    FinalTarget,
)
import pandas as pd

from ..util.functions import serializable
from ..util.config import Provide
from .data_pre import ReadProvideTrainingData, MakeProvidePredictionData


@requires(ReadProvideTrainingData)
class TrainProvideModel(Task):
    def output(self):
        # TODO: add VariableInfluenceMixin
        return {"model": IntermediateTarget(task=self)}

    def run(self):

        # queue up provide configuration
        provide = Provide()

        with self.input().open() as f:
            df = f.read()["InterventionTimelines"]

        # force weird string values to numbers
        df["number_of_recipients"] = pd.to_numeric(
            df["number_of_recipients"], errors="coerce"
        )
        df.dropna(subset=["number_of_recipients"], inplace=True)
        df["number_of_recipients"] = df["number_of_recipients"].astype(int)

        #  calculate duration(in days) and return as integer
        df["duration"] = (
            pd.to_datetime(df["phase_end_date"])
            - pd.to_datetime(df["phase_start_date"])
        ).dt.days

        # normalize names
        df.rename(
            columns={"intervention_location": "admin0"}, inplace=True,
        )

        # The preprocessor handles independent variable transformations
        # TODO: will the predictors be the same for each intervention type?
        preprocessor = ColumnTransformer(
            [
                ("numeric", PowerTransformer(), ["duration", "number_of_recipients"]),
                (
                    "unordered",
                    OneHotEncoder(categories="auto", handle_unknown="ignore"),
                    ["admin0"],
                ),
            ]
        )

        # Use a pipeline to chain independent variable transformations and
        # the selected estimator
        # TODO: include cross-val tuning of hyperparameters, if needed in the
        #       pipeline
        pipeline = Pipeline(
            [("preprocessor", preprocessor), ("estimator", Estimator())]
        )
        # final estimator includes transformation of the dependent variable
        estimator = TransformedTargetRegressor(pipeline, PowerTransformer())

        # drop records w/ missing values
        # TODO: consider imputing
        df.dropna(
            subset=[w for v in preprocessor.transformers for w in v[2]]
            + ["total_cost_USD"],
            inplace=True,
        )

        # fit distinct models for each type of intervention
        x = df.loc[df["intervention_type"] == provide.type, :]
        y = x.pop("total_cost_USD")
        estimator.fit(x, y)

        with self.output()["model"].open("w") as f:
            f.write(estimator)


@requires(TrainProvideModel, MakeProvidePredictionData)
class RunProvideModel(Task):
    """
    """

    def output(self):
        return IntermediateTarget(task=self)

    def run(self):

        with self.input()[0]["model"].open() as f:
            estimator = f.read()

        with self.input()[1].open() as f:
            x = f.read()

        y = pd.DataFrame(index=x.index.copy())

        # predict response point estimate for given intervention parameters
        # at the resolution of the indicator dataset
        y["estimate"] = estimator.predict(x)

        # FIXME: how to make a ML prediction interval w/out parametric
        #     assumptions?
        #     1) bootstrapping the training data
        #     2) "quantile regressor" ... interesting

        y["upper_bound"] = y["estimate"] * 1.1

        y["lower_bound"] = y["estimate"] * 0.9

        with self.output().open("w") as f:
            f.write(y)


@inherits(RunProvideModel)
class Output(Task):
    """
    Communicate the model predictions to the UI.
    """

    # TODO: total cost for the exact parameters in model output

    num = 64
    # TODO: learn valid range from training data or trained model?
    abscissa = {
        # "number_of_recipients": np.linspace(10 ** 4, 10 ** 5, num),
        # "number_of_units": np.linspace(10 ** 5, 10 ** 6, num),
        # "quantity_per_unit": np.linspace(0.1, 10, num),
    }

    def output(self):
        p = Path(*self.task_id.split("."))
        return FinalTarget(path=str(p.with_suffix(".json")))

    def requires(self):
        run_model = {
            k: [self.clone(RunProvideModel, **{k: (x,)}) for x in v.tolist()]
            for k, v in Output.abscissa.items()
        }
        run_model["default"] = self.clone(RunProvideModel)
        return run_model

    def run(self):

        with self.input()["default"].open() as f:
            row = f.read().sum()
        df = row.to_frame().transpose()
        ds = df.to_xarray().squeeze().drop("index")
        ds.attrs["task_family"] = self.task_family
        ds.attrs["parameters"] = self.to_str_params()
        # TODO: units for cost (and everything else)
        dd = ds.to_dict()

        dd["groups"] = {}
        for k, v in Output.abscissa.items():
            agg = []
            for target in self.input()[k]:
                with target.open() as f:
                    row = f.read().sum()
                agg.append(row.to_frame().transpose())
            df = pd.concat(agg)
            df[k] = v
            ds = df.set_index(k).to_xarray()
            ds.attrs["description"] = (
                f"Variables represent the effect of scale parameter {k!r} on "
                f" cost, holding remaining parameters constant at initial"
                f" parameter setting."
            )
            dd["groups"][k] = ds.to_dict()

        dd.update(serializable(self.output()))
        with self.output().open("w") as f:
            json.dump(dd, f, default=serializable)
