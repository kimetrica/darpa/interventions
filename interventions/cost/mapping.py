material_map = {
    "Asphalt Concrete standard": "bitumen",
    "Bituminous concrete": "bitumen",
    "Bitumen standard": "bitumen",
    " bitumen-paved": "bitumen",
    "bituminous standard (Asphalt Hot Mix) road": "bitumen",
    "dual and rehabilitation": "bitumen",
    "Two-lane bitumen": "bitumen",
    "Asphalt concrete standard": "bitumen",
    "Asphalt concrete surface": "bitumen",
    "bitumen": "bitumen",
    "new asphalt surfaced": "bitumen",
    "Dense Bitumen Macadam (DBM) base and Asphalt Concrete (AC)": "bitumen",
    " asphalt concrete": "bitumen",
    "bitumen standard": "bitumen",
    "high class gravel road with intermittent paved sect ions on steep grades": "gravel",
    "all weather high standard asphalt concrete surfaced road": "bitumen",
    "Surface dressing": "bitumen",
    "Asphalt concrete carriage way and double bitumen surface treatment shoulders": "bitumen",
    "earth/gravel road": "gravel",
    "bitumen": "bitumen",
    "bituminous surfacing, very poor physical condition": "bitumen",
    "gravel": "gravel",
    "Gravel": "gravel",
    "Existing paved road": "bitumen",
    "bitumen standard": "bitumen",
}
